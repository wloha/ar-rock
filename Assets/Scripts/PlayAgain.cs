using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayAgain : MonoBehaviour
{
    public Button button;

    void Start()
    {
        button.onClick.AddListener(gameAgain);
    }


    void gameAgain()
    {
        SceneManager.LoadScene("Spawn");
    }
}
