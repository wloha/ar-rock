using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class TimerSlider : MonoBehaviour
{
    GameObject scoreBoardUI;
    TextMeshProUGUI timerText;

    public float timeValue = 10.0f;
    public int displayTime;
    public float multiplier;

    void Start()
    {
        scoreBoardUI = GameObject.FindGameObjectWithTag("ScoreCanvas");
        
        timerText = GameObject.FindGameObjectWithTag("TimerText").GetComponent<TextMeshProUGUI>();
        multiplier = 5.0f;
        Scoring.hiddenScore = 0.0f;
        Scoring.score = 0;

    }

    private void Update()
    {
        if (timeValue > 0.0f)
        {
            timeValue -= Time.deltaTime;

           if(Scoring.hiddenScore >= multiplier)
            {
                timeValue += 8.0f;
                Scoring.hiddenScore -= multiplier;
                multiplier = multiplier * 1.2f;
            }
        }

        else if (timeValue <= 0.0f || Scoring.hiddenScore < 0)
        {
            timeValue = 0;

            SceneManager.LoadScene("GameOver");
        }

        displayTime = Mathf.FloorToInt(timeValue);
        timerText.text = "Time:" + displayTime.ToString();
        
    }
}