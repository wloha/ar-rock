using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

public class Explode : MonoBehaviour
{
    
    public GameObject explosion;
    public AudioSource explodeSound;
    public GameObject enemyToSpawn;
    Vector3 killPos;
    Quaternion killRot;
    public float waitTime = 3.0f;


    void Start()
    {
        explodeSound = GetComponent<AudioSource>();

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Monster")
        {
            Destroy(collision.transform.gameObject);
            
            explodeSound.Play();

            Instantiate(explosion, collision.transform.position, collision.transform.rotation);
            
            Scoring.score += 1;
            Scoring.hiddenScore += 1.0f;

            killPos = collision.transform.position;
            killRot = collision.transform.rotation;
            StartCoroutine(SpawnEnemyAgain());


        }

    }

    IEnumerator SpawnEnemyAgain()
    {
        yield return new WaitForSeconds(waitTime);
        Instantiate(enemyToSpawn, killPos, killRot);
        //bulletCollission = false;
        Destroy(gameObject); // destroy bullet

    }




}
